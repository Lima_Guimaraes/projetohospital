package com.example.walber.projetohospital.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Walber on 08/05/2016.
 */
public class HospitalDbHelper extends SQLiteOpenHelper {
    public static final int DB_VERSION = 1;
    public static final String DB_NAME = "hospital_db";

    public HospitalDbHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE "+ HospitalContract.TABELA_NOME + " ("+
                    HospitalContract.ID            + " INTEGER PRIMARY KEY, "+
                    HospitalContract.BAIRRO        + " TEXT NOT NULL, "+
                    HospitalContract.CENES         + " TEXT NOT NULL, "+
                    HospitalContract.ENDERECO      + " TEXT NOT NULL, "+
                    HospitalContract.ESPECIALIDADE + " TEXT NOT NULL, "+
                    HospitalContract.LATITUDE      + " TEXT NOT NULL, "+
                    HospitalContract.LONGITUDE     + " TEXT NOT NULL, "+
                    HospitalContract.MICRO_REGIAO  + " TEXT NOT NULL, "+
                    HospitalContract.NATUREZA      + " TEXT NOT NULL, "+
                    HospitalContract.RPA           + " TEXT NOT NULL, "+
                    HospitalContract.TELEFONE      + " TEXT NOT NULL, "+
                    HospitalContract.UNIDADE       + " TEXT NOT NULL )");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }
}
