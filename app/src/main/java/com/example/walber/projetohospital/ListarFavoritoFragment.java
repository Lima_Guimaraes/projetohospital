package com.example.walber.projetohospital;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import com.example.walber.projetohospital.database.HospitalDAO;
import com.example.walber.projetohospital.model.Hospital;
import org.greenrobot.eventbus.Subscribe;
import java.util.List;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnItemClick;


public class ListarFavoritoFragment extends Fragment {

    @Bind(R.id.listViewHospital)
    ListView mListViewHospital;

    List<Hospital> mHospitais;
    ArrayAdapter<Hospital> mAdapter;
    HospitalDAO mDAO;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);

        mDAO = new HospitalDAO(getActivity());
        mHospitais = mDAO.listar();

        ((HospitalApp)getActivity().getApplication()).getEventBus().register(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ((HospitalApp)getActivity().getApplication()).getEventBus().unregister(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View layout =  inflater.inflate(R.layout.fragment_lista_favorito, container, false);
        ButterKnife.bind(this, layout);

        mAdapter = new HospitalAdapter(getContext(), mHospitais);

        mListViewHospital.setEmptyView(layout.findViewById(R.id.vazio));

        mListViewHospital.setAdapter(mAdapter);
        return layout;
    }

    @OnItemClick(R.id.listViewHospital)
    void OnItemSelected(int position){
        Hospital hospital= mHospitais.get(position);
        if (getActivity() instanceof CliqueiNoHospitalListener){
            CliqueiNoHospitalListener listener = (CliqueiNoHospitalListener)getActivity();
            listener.hospitalFoiClicado(hospital);
        }
    }

    @Subscribe
    public void atualizar(Hospital hospital){
        mHospitais.clear();
        mHospitais.addAll(mDAO.listar());
        mAdapter.notifyDataSetChanged();
    }
}
