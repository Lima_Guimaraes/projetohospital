package com.example.walber.projetohospital.model;

import org.parceler.Parcel;

/**
 * Created by Walber on 01/05/2016.
 */
@Parcel
public class Hospital {

    long id;
    String natureza;
    String cnes;
    String unidade;
    String rpa;
    String bairro;
    String micro_regiao;
    String endereco;
    String especialidade;
    String telefone;
    String longitude;
    String latitude;

    public Hospital(int id, String natureza, String cnes, String unidade, String rpa, String bairro, String micro_regiao, String endereco, String especialidade, String telefone, String longitude, String latitude) {
        this.id = id;
        this.natureza = natureza;
        this.cnes = cnes;
        this.unidade = unidade;
        this.rpa = rpa;
        this.bairro = bairro;
        this.micro_regiao = micro_regiao;
        this.endereco = endereco;
        this.especialidade = especialidade;
        this.telefone = telefone;
        this.longitude = longitude;
        this.latitude = latitude;
    }

    public long getId() {

        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNatureza() {
        return natureza;
    }

    public void setNatureza(String natureza) {
        this.natureza = natureza;
    }

    public String getCnes() {
        return cnes;
    }

    public void setCnes(String cnes) {
        this.cnes = cnes;
    }

    public String getUnidade() {
        return unidade;
    }

    public void setUnidade(String unidade) {
        this.unidade = unidade;
    }

    public String getRpa() {
        return rpa;
    }

    public void setRpa(String rpa) {
        this.rpa = rpa;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public String getMicro_regiao() {
        return micro_regiao;
    }

    public void setMicro_regiao(String micro_regiao) {
        this.micro_regiao = micro_regiao;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public String getEspecialidade() {
        return especialidade;
    }

    public void setEspecialidade(String especialidade) {
        this.especialidade = especialidade;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public Hospital(){}

    @Override
    public String toString() {
        return "Hospital{" +
                natureza + '\'' + unidade + '\'' +
                '}';
    }
}
