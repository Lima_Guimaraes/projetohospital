package com.example.walber.projetohospital;


import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.example.walber.projetohospital.model.CentralHospital;
import com.example.walber.projetohospital.model.Hospital;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnItemClick;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;


public class ListarHospitalFragment extends Fragment {

    @Bind(R.id.listViewHospital)
    ListView mListViewHospital;
    @Bind(R.id.swipe)
    SwipeRefreshLayout mSwipe;
    List<Hospital> mHospitais;
    ArrayAdapter<Hospital> mAdapter;
    CentralHospital mCentralHospital;
    HospitalTask mHospitalTask;
    int hospitalLocalizacao;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setRetainInstance(true);
        mHospitais = new ArrayList<>();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if ((mHospitais.size() == 0) && (mHospitalTask == null)) {
            baixarJson();
        }else if (mHospitalTask != null && mHospitalTask.getStatus() == AsyncTask.Status.RUNNING){
            showProgress();
        }

        if (getResources().getBoolean(R.bool.tablet) && mHospitais.size() > 0) {
            {
                OnItemSelected(hospitalLocalizacao);
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View layout =  inflater.inflate(R.layout.fragment_lista_hospital, container, false);
        ButterKnife.bind(this, layout);

        mAdapter = new HospitalAdapter(getContext(), mHospitais);
        mListViewHospital.setAdapter(mAdapter);

        mListViewHospital.setEmptyView(layout.findViewById(R.id.vazio));

        mSwipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                baixarJson();
            }
        });
        return layout;
    }

    private void baixarJson(){
        ConnectivityManager cm = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info = cm.getActiveNetworkInfo();

        if (info != null && info.isConnected()) {
            mHospitalTask = new HospitalTask();
            mHospitalTask.execute();
        }else{
            mSwipe.setRefreshing(false);
            Toast.makeText(getActivity(), R.string.erro_conexao, Toast.LENGTH_SHORT).show();
        }
    }

    private void showProgress(){
        mSwipe.post(new Runnable() {
            @Override
            public void run() {
                mSwipe.setRefreshing(true);
            }
        });
    }

    @OnItemClick(R.id.listViewHospital)
    void OnItemSelected(int position){
        Hospital hospital= mHospitais.get(position);
        if (getActivity() instanceof CliqueiNoHospitalListener){
            CliqueiNoHospitalListener listener = (CliqueiNoHospitalListener)getActivity();
            listener.hospitalFoiClicado(hospital);
            hospitalLocalizacao = position;
        }
    }

    class HospitalTask extends AsyncTask<Void, Void, CentralHospital>{

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgress();
        }

        @Override
        protected CentralHospital doInBackground(Void... params) {

            OkHttpClient client = new OkHttpClient();

            Request request = new Request.Builder()
                        .url("http://dados.recife.pe.gov.br/api/action/datastore_search?resource_id=2db3a931-7cf2-4974-9c06-c9fc99b5f5af&")
                        .build();
            try {

                Response response = client.newCall(request).execute();
                String jsonString = response.body().string();
                Log.d("NGDL", jsonString);

                List<Hospital> hospitais = new ArrayList<>();
                mCentralHospital = new CentralHospital();

                JSONObject jsonObject = new JSONObject(jsonString);
                JSONObject jsonResult = jsonObject.getJSONObject("result");
                JSONArray jsonResults = jsonResult.getJSONArray("records");

                for (int i = 0; i < jsonResults.length(); i++) {
                    Hospital hospital = new Hospital();
                    JSONObject jsonRecord = jsonResults.getJSONObject(i);
                    hospital.setId(jsonRecord.getInt("_id"));
                    hospital.setNatureza(jsonRecord.getString("natureza"));
                    hospital.setCnes(jsonRecord.getString("cnes"));
                    hospital.setUnidade(jsonRecord.getString("unidade"));
                    hospital.setLongitude(jsonRecord.getString("longitude"));
                    hospital.setRpa(jsonRecord.getString("rpa"));
                    hospital.setLatitude(jsonRecord.getString("latitude"));
                    hospital.setEspecialidade(jsonRecord.getString("especialidades"));
                    hospital.setTelefone(jsonRecord.getString("telefone"));
                    hospital.setBairro(jsonRecord.getString("bairro"));
                    hospital.setMicro_regiao(jsonRecord.getString("micro_regiao"));
                    hospital.setEndereco(jsonRecord.getString("endereco"));

                    hospitais.add(hospital);
                }
                mCentralHospital.setHospitals((hospitais));
                return mCentralHospital;

            }catch (Exception e){
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(CentralHospital centralHospital) {
            super.onPostExecute(centralHospital);
            if(centralHospital != null){
                for (Hospital hospital: centralHospital.getHospitals()) {
                     mHospitais.add(hospital);
                }
                mAdapter.notifyDataSetChanged();

                if (getResources().getBoolean(R.bool.tablet) && mHospitais.size() > 0){
                    {
                        OnItemSelected(0);
                    }
                }
            }
            mSwipe.setRefreshing(false);
        }
    }
}