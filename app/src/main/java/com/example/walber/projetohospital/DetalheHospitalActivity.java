package com.example.walber.projetohospital;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.example.walber.projetohospital.model.Hospital;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.parceler.Parcels;

public class DetalheHospitalActivity extends AppCompatActivity {

    public static final String EXTRA_HOSPITAL = "jogo";
    GoogleMap mGoogleMap;
    LatLng mLocalizacao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalhe_hospital);

        Hospital hospital = Parcels.unwrap( getIntent().getParcelableExtra(EXTRA_HOSPITAL) );;
        DetalheHospitalFragment dof = DetalheHospitalFragment.newInstance(hospital);
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.detalhe, dof, "detalhe")
                .commit();


        SupportMapFragment map = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.maps);
        mGoogleMap = map.getMap();
        mGoogleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        mLocalizacao = new LatLng(Double.valueOf(hospital.getLatitude()), Double.valueOf(hospital.getLongitude()));
        mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(mLocalizacao, 17.0f));
        mGoogleMap.addMarker(new MarkerOptions()
                .position(mLocalizacao)
                .title(hospital.getUnidade())
                .snippet(hospital.getEspecialidade()));
    }
}
