package com.example.walber.projetohospital;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.walber.projetohospital.model.Hospital;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Walber on 04/05/2016.
 */
public class HospitalAdapter extends ArrayAdapter<Hospital> {
    public HospitalAdapter(Context context, List<Hospital> hospitais) {
        super(context, 0, hospitais);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Hospital hospital = getItem(position);
        ViewHolder viewHolder;

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext())
                    .inflate(R.layout.item_hospital, parent, false);
            viewHolder = new ViewHolder(convertView);
        }else{
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.mTextViewUnidade.setText(hospital.getUnidade());
        viewHolder.mTextViewEspecialidade.setText(hospital.getEspecialidade());

        return convertView;
    }

    static class ViewHolder{
        @Bind(R.id.textView_Unidade)
        TextView mTextViewUnidade;
        @Bind(R.id.textView_Especialidade)
        TextView mTextViewEspecialidade;

        public ViewHolder(View parent){
            ButterKnife.bind(this, parent);
            parent.setTag(this);
        }
    }
}
