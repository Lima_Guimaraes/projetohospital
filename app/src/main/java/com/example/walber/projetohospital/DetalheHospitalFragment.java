package com.example.walber.projetohospital;


import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.ShareActionProvider;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.walber.projetohospital.database.HospitalDAO;
import com.example.walber.projetohospital.model.Hospital;

import org.parceler.Parcels;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DetalheHospitalFragment extends Fragment{

    private static final String EXTRA_HOSPITAL = "param1";
    private Hospital mHospital;

    @Bind(R.id.textView_Unidade)
    TextView mTextViewUnidade;
    @Bind(R.id.textView_Especialidade)
    TextView mTextViewEspecilidade;
    @Bind(R.id.textView_Endereco)
    TextView mTextViewEndereco;
    @Bind(R.id.textView_Bairro)
    TextView mTextViewBairro;
    @Bind(R.id.textView_Telefone)
    TextView mTextViewTelefone;
    @Bind(R.id.fab_favorito)
    FloatingActionButton mFabFavorito;

    HospitalDAO mDAO;
    private ShareActionProvider mShareActionProvider;


    public DetalheHospitalFragment() {

    }

    //Recebe o Hospital como Parâmetro
    //Armazena no Bundle
    //Coloca no set Arguments
    //Parcels.wrap envolve o Hospital no Parcelable
    public static DetalheHospitalFragment newInstance(Hospital hospital) {
        DetalheHospitalFragment fragment = new DetalheHospitalFragment();
        Bundle args = new Bundle();
        Parcelable parcelable = Parcels.wrap(hospital);
        args.putParcelable(EXTRA_HOSPITAL, parcelable);
        fragment.setArguments(args);
        return fragment;
    }

    //Ler o Hospital como Parâmetro
    //setHasOptionMenu - É a permissão dado para fragment implementar o menu.
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);
        mDAO = new HospitalDAO(getActivity());
        if (getArguments() != null) {
            Parcelable parcelable = getArguments().getParcelable(EXTRA_HOSPITAL);
            mHospital = Parcels.unwrap(parcelable);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_detalhe_hospital, container, false);
        ButterKnife.bind(this, view);

        mTextViewUnidade.setText(getString(R.string.unidade, mHospital.getUnidade()));
        mTextViewEspecilidade.setText(getString(R.string.especialidade, mHospital.getEspecialidade()));
        mTextViewEndereco.setText(getString(R.string.endereco, mHospital.getEndereco()));
        mTextViewBairro.setText(getString(R.string.bairro, mHospital.getBairro()));
        mTextViewTelefone.setText(getString(R.string.telefone, mHospital.getTelefone()));
        toggleFavorito();
        return view;
    }

    private void toggleFavorito(){
        boolean favorito = mDAO.isFavorito(mHospital);

        mFabFavorito.setImageResource(
                favorito ? R.drawable.ic_remove : R.drawable.ic_check);

        mFabFavorito.setBackgroundTintList(
                favorito ? ColorStateList.valueOf(Color.RED) : ColorStateList.valueOf(Color.BLUE));
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_detalhe, menu);

        MenuItem item = menu.findItem(R.id.menu_item_share);

        mShareActionProvider = (ShareActionProvider) MenuItemCompat.getActionProvider(item);

        Intent it = new Intent(Intent.ACTION_SEND);
        it.putExtra(Intent.EXTRA_TEXT, mHospital.getUnidade());
        it.setType("text/plain");

        mShareActionProvider.setShareIntent(it);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @OnClick(R.id.fab_favorito)
    public void favoritoClick(){
        if (mDAO.isFavorito(mHospital)){
            mDAO.excluir(mHospital);
        }else{
            mDAO.inserir(mHospital);
        }

        mFabFavorito.animate()
                .scaleX(0)
                .scaleY(0)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        toggleFavorito();
                        mFabFavorito.animate().scaleX(1).scaleY(1).setListener(null);
                    }
                });
        ((HospitalApp)getActivity().getApplication()).getEventBus().post(mHospital);
    }
}
