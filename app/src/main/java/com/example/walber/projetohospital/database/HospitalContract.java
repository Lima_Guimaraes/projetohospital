package com.example.walber.projetohospital.database;

import android.provider.BaseColumns;

/**
 * Created by Walber on 08/05/2016.
 */
public interface HospitalContract extends BaseColumns {
    String TABELA_NOME = "hospital";

    String ID = "id";
    String BAIRRO = "bairro";
    String CENES = "cnes";
    String ENDERECO = "endereco";
    String ESPECIALIDADE = "especialidade";
    String LATITUDE = "latitude";
    String LONGITUDE = "longitude";
    String MICRO_REGIAO = "micro_regiao";
    String NATUREZA = "natureza";
    String RPA = "rpa";
    String TELEFONE = "telefone";
    String UNIDADE = "unidade";
}
