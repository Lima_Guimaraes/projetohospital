package com.example.walber.projetohospital.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.example.walber.projetohospital.model.Hospital;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Walber on 09/05/2016.
 */
public class HospitalDAO {

    private Context mContext;

    public HospitalDAO(Context context){
        this.mContext = context;
    }

    //Retorno Long, é porque é o ID que está sendo inserido.
    public long inserir(Hospital hospital){
        HospitalDbHelper db = new HospitalDbHelper(mContext);
        SQLiteDatabase sqlLiteDataBase = db.getWritableDatabase();

        ContentValues contentValues = valuesFromHospital(hospital);
        long id = sqlLiteDataBase.insert(HospitalContract.TABELA_NOME, null, contentValues);

        sqlLiteDataBase.close();

        return id;
    }

    //Retorno é INT, por informa a quantidade linhas que foram afetadas.
    public int atualizar(Hospital hospital){
        HospitalDbHelper db = new HospitalDbHelper(mContext);
        SQLiteDatabase sqlLiteDataBase = db.getWritableDatabase();

        ContentValues contentValues = valuesFromHospital(hospital);
        int linhasAfetadas = sqlLiteDataBase.update(HospitalContract.TABELA_NOME, contentValues,
                                                    HospitalContract.ID + "= ?",
                                                    new String[]{ String.valueOf(hospital.getId())});
        sqlLiteDataBase.close();

        return linhasAfetadas;
    }

    public int excluir(Hospital hospital){
        HospitalDbHelper db = new HospitalDbHelper(mContext);
        SQLiteDatabase sqlLiteDataBase = db.getWritableDatabase();

        int linhasAfetadas = sqlLiteDataBase.delete(HospitalContract.TABELA_NOME,
                                                    HospitalContract.ID + "= ?",
                                                     new String[]{ String.valueOf(hospital.getId())});
        sqlLiteDataBase.close();
        return linhasAfetadas;
    }

    public List<Hospital> listar(){
        HospitalDbHelper db = new HospitalDbHelper(mContext);
        SQLiteDatabase sqlLiteDataBase = db.getReadableDatabase();

        Cursor cursor = sqlLiteDataBase.rawQuery("SELECT * FROM " + HospitalContract.TABELA_NOME, null);
        List<Hospital> hospitais = new ArrayList<>();

        if (cursor.getCount() > 0) {

            int idxId = cursor.getColumnIndex(HospitalContract.ID);
            int idxNatureza = cursor.getColumnIndex(HospitalContract.NATUREZA);
            int idxCnes = cursor.getColumnIndex(HospitalContract.CENES);
            int idxUnidade = cursor.getColumnIndex(HospitalContract.UNIDADE);
            int idxRpa = cursor.getColumnIndex(HospitalContract.RPA);
            int idxBairro = cursor.getColumnIndex(HospitalContract.BAIRRO);
            int idxMicro_regiao = cursor.getColumnIndex(HospitalContract.MICRO_REGIAO);
            int idxEndereco = cursor.getColumnIndex(HospitalContract.ENDERECO);
            int idxEspecialidade = cursor.getColumnIndex(HospitalContract.ESPECIALIDADE);
            int idxTelefone = cursor.getColumnIndex(HospitalContract.TELEFONE);
            int idxLongitude = cursor.getColumnIndex(HospitalContract.LONGITUDE);
            int idxLatitude = cursor.getColumnIndex(HospitalContract.LATITUDE);

            while (cursor.moveToNext()) {
                Hospital hospital = new Hospital();

                hospital.setId(cursor.getLong(idxId));
                hospital.setNatureza(cursor.getString(idxNatureza));
                hospital.setCnes(cursor.getString(idxCnes));
                hospital.setUnidade(cursor.getString(idxUnidade));
                hospital.setRpa(cursor.getString(idxRpa));
                hospital.setBairro(cursor.getString(idxBairro));
                hospital.setMicro_regiao(cursor.getString(idxMicro_regiao));
                hospital.setEndereco(cursor.getString(idxEndereco));
                hospital.setEspecialidade(cursor.getString(idxEspecialidade));
                hospital.setTelefone(cursor.getString(idxTelefone));
                hospital.setLongitude(cursor.getString(idxLongitude));
                hospital.setLatitude(cursor.getString(idxLatitude));

                hospitais.add(hospital);
            }
            cursor.close();
        }
        sqlLiteDataBase.close();
        return hospitais;
    }

    public boolean isFavorito(Hospital hospital){
        HospitalDbHelper db = new HospitalDbHelper(mContext);
        SQLiteDatabase sqlLiteDataBase = db.getReadableDatabase();

        Cursor cursor = sqlLiteDataBase.rawQuery("SELECT COUNT(*) FROM " + HospitalContract.TABELA_NOME +
                                                 " WHERE " + HospitalContract.ID + " = ?"
                                                 , new String[]{String.valueOf(hospital.getId())});
        boolean existe = false;
        if (cursor !=null) {
            cursor.moveToNext();
            existe = cursor.getInt(0) > 0;
            cursor.close();
        }
        db.close();
        return existe;
    }

    private ContentValues valuesFromHospital(Hospital hospital){
        ContentValues contentValues = new ContentValues();

        contentValues.put(HospitalContract.ID, hospital.getId());
        contentValues.put(HospitalContract.BAIRRO, hospital.getBairro());
        contentValues.put(HospitalContract.CENES, hospital.getCnes());
        contentValues.put(HospitalContract.ENDERECO, hospital.getEndereco());
        contentValues.put(HospitalContract.ESPECIALIDADE, hospital.getEspecialidade());
        contentValues.put(HospitalContract.LATITUDE, hospital.getLatitude());
        contentValues.put(HospitalContract.MICRO_REGIAO, hospital.getMicro_regiao());
        contentValues.put(HospitalContract.LONGITUDE, hospital.getLongitude());
        contentValues.put(HospitalContract.NATUREZA, hospital.getNatureza());
        contentValues.put(HospitalContract.RPA, hospital.getRpa());
        contentValues.put(HospitalContract.TELEFONE, hospital.getTelefone());
        contentValues.put(HospitalContract.UNIDADE, hospital.getUnidade());

        return contentValues;
    }
}
