package com.example.walber.projetohospital;

import android.app.Application;

import org.greenrobot.eventbus.EventBus;

/**
 * Created by Walber on 22/05/2016.
 */
public class HospitalApp extends Application {

    EventBus mEventBus;

    @Override
    public void onCreate() {
        super.onCreate();

        mEventBus = new EventBus();
    }

    public EventBus getEventBus(){
        return mEventBus;
    }
}
