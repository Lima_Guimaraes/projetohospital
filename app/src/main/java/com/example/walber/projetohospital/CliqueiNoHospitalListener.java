package com.example.walber.projetohospital;

import com.example.walber.projetohospital.model.Hospital;

interface CliqueiNoHospitalListener {
        void hospitalFoiClicado(Hospital hospital);
}