package com.example.walber.projetohospital.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Walber on 01/05/2016.
 */
public class CentralHospital {

    @SerializedName("result")
    List<Hospital> hospitals;

    public List<Hospital> getHospitals() {
        return hospitals;
    }

    public void setHospitals(List<Hospital> hospitals) {
        this.hospitals = hospitals;
    }
}
