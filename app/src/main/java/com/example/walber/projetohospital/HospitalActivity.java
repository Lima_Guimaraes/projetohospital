package com.example.walber.projetohospital;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.example.walber.projetohospital.model.Hospital;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.parceler.Parcels;

import butterknife.Bind;
import butterknife.ButterKnife;

public class HospitalActivity extends AppCompatActivity
        implements CliqueiNoHospitalListener {

    @Bind(R.id.viewPager)
    ViewPager mViewPager;
    @Bind(R.id.tabLayout)
    TabLayout mTabLayout;
    @Bind(R.id.toolBar)
    Toolbar mToolbar;
    Hospital mHospital;
    GoogleMap mGoogleMap;
    LatLng mLocalizacao;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hospital);
        ButterKnife.bind(this);
        mViewPager.setAdapter(new HospitalViewPager(getSupportFragmentManager()));

        setSupportActionBar(mToolbar);
        mTabLayout.setupWithViewPager(mViewPager);
    }

    class HospitalViewPager extends FragmentPagerAdapter{
        public HospitalViewPager(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            if (position == 0) return new ListarHospitalFragment();
            return new ListarFavoritoFragment();
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            if (position ==0) return getString(R.string.aba_web);
            return getString(R.string.aba_favorito);
        }
    }

    @Override
    public void hospitalFoiClicado(Hospital hospital) {

        if (getResources().getBoolean(R.bool.tablet)) {
            DetalheHospitalFragment dof = DetalheHospitalFragment.newInstance(hospital);
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.detalhe, dof, "detalhe")
                    .commit();

            mHospital = hospital;

            SupportMapFragment map = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.maps);
            mGoogleMap = map.getMap();
            mGoogleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
            mLocalizacao = new LatLng(Double.valueOf(mHospital.getLatitude()), Double.valueOf(mHospital.getLongitude()));
            mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(mLocalizacao, 17.0f));
            mGoogleMap.addMarker(new MarkerOptions()
                    .position(mLocalizacao)
                    .title(mHospital.getUnidade())
                    .snippet(mHospital.getEspecialidade()));

        } else{
            Intent it = new Intent(this, DetalheHospitalActivity.class);
            Parcelable parcelable = Parcels.wrap(hospital);
            it.putExtra(DetalheHospitalActivity.EXTRA_HOSPITAL, parcelable);
            startActivity(it);
        }
    }
}
